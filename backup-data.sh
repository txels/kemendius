#!/bin/bash

kubectl cp $(kubectl get pod -l app=kemendius-be -o name | head -n 1 | cut -c 5-):/data backup
