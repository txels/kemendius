module.exports = {
  theme: {
    extend: {
      fontFamily: {
        sans: [
          // "-apple-system",
          // "BlinkMacSystemFont",
          // '"Segoe UI"',
          // "Roboto",
          '"Helvetica Neue"',
          "Arial",
          // '"Noto Sans"',
          "sans-serif"
          // '"Apple Color Emoji"',
          // '"Segoe UI Emoji"',
          // '"Segoe UI Symbol"',
          // '"Noto Color Emoji"'
        ],
        serif: [
          "Georgia",
          // "Cambria",
          '"Times New Roman"',
          "Times",
          "serif"
        ],
        mono: [
          // "Menlo",
          // "Monaco",
          "Consolas",
          // '"Liberation Mono"',
          '"Ubuntu Mono"',
          "monospace"
        ]
      },
      fontSize: {
        xxs: "0.6rem"
      },
      variants: {
        backgroundColor: ["disabled", "hover", "active", "focus"],
        borderColor: ["hover", "focus"],
        borderWidth: ["responsive", "focus"],
        cursor: ["responsive", "disabled"],
        opacity: ["responsive", "hover", "focus", "disabled"],
        position: ["responsive"],
        zIndex: ["responsive", "hover"]
      }
    }
  },
  plugins: []
};
