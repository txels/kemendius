const TEXT = "This is a sample text";
const COMMENT = "This is a sample comment";

describe("Upload an image for feedback", () => {
  it("Shows a form to request feedback, focused", () => {
    cy.visit("/");
    cy.contains("Upload an image").click();

    cy.contains("Upload an image for feedback");
    cy.get("body")
      .find("input[type=file]")
      .should("have.focus")
      .and("be.empty");
  });

  it("Send is disabled if there is no file", () => {
    cy.get("button[type=submit]").should("be.disabled");
  });

  it("You can upload a file", () => {
    // Our file upload mechanism in e2e doesn't trigger the
    // file input change event, thus not enabling the submit button.
    // The e2e query param is a hack so that the submit button is enabled
    cy.visit("#/image?e2e=true");

    cy.get("input[type=file]");
    cy.uploadFile(
      "images/photo.jpg",
      "photo.jpg",
      "image/jpeg",
      "input[type=file]"
    );
    cy.get("button[type=submit]").click();
  });

  it("After submission, shows the image with no comments and an empty COMMENT form", () => {
    cy.contains("I would love your feedback on this image.");
    cy.get(".highlightable img");
    cy.get(".highlightable canvas");
    cy.get("body")
      .get(".comment")
      .should("have.length", 0);
    cy.contains("Add a comment");
    cy.get("textarea").should("be.empty");
  });

  it("Send is disabled if there is no comment text", () => {
    cy.get("button[type=submit]").should("be.disabled");
  });

  it("You can submit a comment with 'shift-enter'", () => {
    cy.get("#comment-form textarea")
      .focus()
      .type(COMMENT)
      .type("{shift}{enter}");
  });

  it("After submission, shows the item with one comment", () => {
    cy.contains(COMMENT);
    cy.get("body")
      .get(".comment")
      .should("have.length", 1);
  });

  it("After submission, comment is empty again and send is disabled", () => {
    cy.get("textarea").should("be.empty");
    cy.get("button[type=submit]").should("be.disabled");
  });

  it("Clicking on upload an image takes us back to an empty form", () => {
    cy.contains("Upload an image").click();
    cy.get("body")
      .find("input[type=file]")
      .should("have.focus")
      .and("be.empty");
  });
});
