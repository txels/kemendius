const TEXT = "This is a sample text";
const COMMENT = "This is a sample comment";

describe("Post a text for feedback", () => {
  it("Shows a form to request feedback, focused", () => {
    cy.visit("/");
    cy.contains("Post a text").click();

    cy.contains("Enter text for feedback");
    cy.get("body")
      .find("textarea")
      .should("have.focus")
      .and("be.empty");
  });

  it("Send is disabled if there is no text", () => {
    cy.get("button[type=submit]").should("be.disabled");
  });

  it("You can submit text with 'shift-enter'", () => {
    cy.focused()
      .type(TEXT)
      .type("{shift}{enter}");
  });

  it("After submission, shows the item with no comments and an empty COMMENT form", () => {
    cy.contains(".submission", TEXT);
    cy.get("body")
      .get(".comment")
      .should("have.length", 0);
    cy.contains("Add a comment");
    cy.get("textarea").should("be.empty");
  });

  it("Send is disabled if there is no comment text", () => {
    cy.get("button[type=submit]").should("be.disabled");
  });

  it("You can submit a comment with 'shift-enter'", () => {
    cy.get("#comment-form textarea")
      .focus()
      .type(COMMENT)
      .type("{shift}{enter}");
  });

  it("After submission, shows the item with one comment", () => {
    cy.contains(COMMENT);
    cy.get("body")
      .get(".comment")
      .should("have.length", 1);
  });

  it("After submission, comment is empty again and send is disabled", () => {
    cy.get("textarea").should("be.empty");
    cy.get("button[type=submit]").should("be.disabled");
  });

  it("Clicking on create new item takes us back to an empty form", () => {
    cy.contains("Post a text").click();
    cy.get("textarea").should("be.empty");
    cy.get("button[type=submit]").should("be.disabled");
    cy.get(".markdown").should("be.empty");
  });
});
