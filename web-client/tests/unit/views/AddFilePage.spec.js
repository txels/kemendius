import { shallowMount, createLocalVue } from "@vue/test-utils";

import AddFilePage from "@/views/AddFilePage.vue";

const factory = () => {
  return shallowMount(AddFilePage, {});
};

describe("AddFilePage.vue", () => {
  it("Displays a form (stubbed)", () => {
    const wrapper = factory();
    let el = wrapper.element;
    expect(el.outerHTML).toContain("<fileuploadform-stub>");
  });
});
