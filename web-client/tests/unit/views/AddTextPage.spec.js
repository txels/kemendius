import { shallowMount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import * as config from "@/store/config";
import { cloneDeep } from "lodash";

import AddTextPage from "@/views/AddTextPage.vue";

const factory = id => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(config));
  const $route = {
    path: "/",
    params: { id: id }
  };
  return shallowMount(AddTextPage, {
    localVue,
    store
  });
};

describe("AddTextPage.vue", () => {
  it("Displays a form (stubbed)", () => {
    const wrapper = factory(null);
    let el = wrapper.element;
    expect(wrapper.item).toBeUndefined();
    expect(el.outerHTML).toEqual(
      "<feedback-item-form-stub></feedback-item-form-stub>"
    );
  });
});
