import { mount, createLocalVue } from "@vue/test-utils";
import { cloneDeep } from "lodash";
import Vuex from "vuex";
import flushPromises from "flush-promises";

import {} from "@/vue-init"; // global directives + components
import * as config from "@/store/config";
import ItemsBackend from "@/services/ItemsBackend";
import TextFeedbackPage from "@/views/TextFeedbackPage.vue";

jest.mock("@/services/ItemsBackend");

beforeEach(() => {
  jest.clearAllMocks();
});

const factory = id => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(config));
  const $route = {
    path: "/",
    params: { id: id }
  };
  const $window = {
    location: "dummy"
  };

  return mount(TextFeedbackPage, {
    localVue,
    store,
    mocks: { $window },
    propsData: { id },
    stubs: ["router-link"]
  });
};

const UID = "e14a9ee5-be2e-4dc8-bf9a-c795aadbc506";

describe("FeedbackItem.vue", () => {
  it("Starts in a loading state", () => {
    const wrapper = factory(UID);
    let el = wrapper.element;
    expect(wrapper.item).toBeUndefined();
    expect(el.textContent.trim()).toEqual("Loading...");
  });

  it("A backend error results in a not found", async () => {
    ItemsBackend.getItem.mockRejectedValueOnce();

    const wrapper = factory(UID);

    await flushPromises();
    let el = wrapper.element;
    expect(el.textContent.trim()).toEqual("Not Found");
  });

  it("Loads an item upon creation", async () => {
    ItemsBackend.getItem.mockResolvedValue({
      data: { id: UID, text: "# Hello" }
    });
    ItemsBackend.getComments.mockResolvedValue({
      data: [{ text: "comment one" }]
    });

    const wrapper = factory(UID);

    await flushPromises();
    expect(wrapper.element.innerHTML).toContain(">Hello<");
    expect(wrapper.element.innerHTML).toContain(">comment one<");
  });
});
