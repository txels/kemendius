import { shallowMount } from "@vue/test-utils";

import {} from "@/vue-init"; // global directives + components
import HighlightMenu from "@/components/HighlightMenu.vue";

const factory = (props = {}) => {
  let propsData = {
    highlight: {},
    x: 100,
    y: 100,
    ...props
  };
  return shallowMount(HighlightMenu, {
    propsData
  });
};

describe("HiglightMenu.vue", () => {
  describe("menu items", () => {
    it("when menu is empty, it contains no buttons", () => {
      const wrapper = factory({});

      expect(wrapper.html()).toBe("");
    });
    it("menu has at least a close button", () => {
      const wrapper = factory({
        menu: {}
      });

      expect(wrapper.get("[name=close-outlined").html()).toBe(
        '<baseicon-stub color="text-white" name="close-outlined" width="24" height="24"></baseicon-stub>'
      );
    });
    it("clicking on the close button emits a menu-cancel event", async () => {
      const wrapper = factory({
        menu: {}
      });
      let closeIcon = wrapper.get("[name=close-outlined");

      closeIcon.trigger("click");
      await wrapper.vm.$nextTick();
      expect(wrapper.emitted("menu-cancel").length).toBe(1);
    });

    it("menu can have a comments button", () => {
      const wrapper = factory({
        menu: { comment: true }
      });

      expect(wrapper.get("[name=comments").html()).toBe(
        '<baseicon-stub color="text-white" name="comments" width="24" height="24"></baseicon-stub>'
      );
      expect(wrapper.get("[name=close-outlined").html()).toBe(
        '<baseicon-stub color="text-white" name="close-outlined" width="24" height="24"></baseicon-stub>'
      );
    });
    it("a textarea is shown only when clicking the comments button", async () => {
      const wrapper = factory({
        menu: { comment: true }
      });
      let commentButton = wrapper.get("[name=comments");
      expect(wrapper.contains("textarea")).toBe(false);

      commentButton.trigger("click");
      await wrapper.vm.$nextTick();

      expect(wrapper.contains("textarea")).toBe(true);
    });
    it("menu can have additional buttons", () => {
      const wrapper = factory({
        menu: {
          actions: [
            {
              icon: "marker",
              color: "text-yellow-400"
            }
          ],
          comment: true
        }
      });

      expect(wrapper.get("[name=marker").html()).toBe(
        '<baseicon-stub color="text-yellow-400" name="marker" width="24" height="24"></baseicon-stub>'
      );
    });
    it("menu items may have callbacks that are called upon click", async () => {
      let clicked = null;
      const wrapper = factory({
        highlight: { text: "Hello" },
        menu: {
          actions: [
            {
              icon: "marker",
              color: "text-yellow-400",
              callback: item => {
                clicked = item;
              }
            }
          ],
          comment: true
        }
      });

      expect(clicked).toBe(null);
      wrapper.get("[name=marker]").trigger("click");
      await wrapper.vm.$nextTick();

      expect(clicked.text).toBe("Hello");
      expect(wrapper.emitted("menu-clicked").length).toBe(1);
    });
  });
});
