import { shallowMount } from "@vue/test-utils";

import {} from "@/vue-init";
import FeedbackItem from "@/components/FeedbackItem.vue";

const factory = (props = {}) => {
  let propsData = {
    item: {
      text: "Default text",
      id: "1",
      rendered: "<p>Default text</p>",
      comments: []
    },
    ...props
  };
  return shallowMount(FeedbackItem, {
    propsData,
    stubs: ["router-link"]
  });
};

describe("FeedbackItem.vue", () => {
  it("Asks for feedback for a feedback item, if present", () => {
    const wrapper = factory();

    let heading = wrapper.find("h2");
    expect(heading.text()).toEqual("I would love your feedback on this text.");
  });
});
