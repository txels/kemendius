import { shallowMount } from "@vue/test-utils";

import {} from "@/vue-init"; // global directives + components
import ActiveUsers from "@/components/ActiveUsers.vue";

const factory = viewers => {
  return shallowMount(ActiveUsers, { propsData: { viewers: viewers } });
};

describe("ActiveUsers.vue", () => {
  it("if viewers==0, displays nothing", () => {
    const wrapper = factory(0);

    expect(() => wrapper.get("p")).toThrow();
  });
  it("if viewers==1, use user icon and singular", () => {
    const wrapper = factory(1);

    const p = wrapper.get("p");
    expect(p.text().replace(/\s+/g, " ")).toContain("1 more viewer");
    expect(p.html()).toContain('"/icons.svg#user"');
  });
  it("if viewers>1, use user-group icon and plural", () => {
    const wrapper = factory(3);

    const p = wrapper.get("p");
    expect(p.text().replace(/\s+/g, " ")).toContain("3 more viewers");
    expect(p.html()).toContain("svg");
    expect(p.html()).toContain('"/icons.svg#user-group"');
  });
});
