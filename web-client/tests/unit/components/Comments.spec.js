import { mount } from "@vue/test-utils";

import {} from "@/vue-init"; // global directives + components
import Comments from "@/components/Comments.vue";

const factory = () => {
  return mount(Comments, { propsData: { comments: [] } });
};

describe("Comments.vue", () => {
  it("displays a comment form with a textarea and a submit button", () => {
    const wrapper = factory();

    const textarea = wrapper.get("textarea");
    const button = wrapper.get("button");
    expect(textarea.attributes("placeholder")).toBe("Don't be shy...");
    expect(button.attributes("type")).toBe("submit");
  });

  it("the button is disabled if there is no comment text", () => {
    const wrapper = factory();

    const button = wrapper.get("button");
    expect(button.attributes("disabled")).toBe("disabled");
  });

  it("the button is enabled if there is some text", async () => {
    const wrapper = factory();

    const textarea = wrapper.get("textarea");
    textarea.setValue("Hello");
    await wrapper.vm.$nextTick();

    const button = wrapper.get("button");
    expect(button.attributes("disabled")).toBeUndefined();
  });

  it("upon form submission, an event is emitted", async () => {
    const wrapper = factory();
    wrapper.setData({ comment: { text: "Hello" } });

    wrapper.get("form").trigger("submit");

    expect(wrapper.emitted("comment-submitted")).toEqual([[{ text: "Hello" }]]);
  });
});
