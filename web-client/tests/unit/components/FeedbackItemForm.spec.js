import Vue from "vue";
import { mount } from "@vue/test-utils";

import FeedbackItemForm from "@/components/FeedbackItemForm.vue";
// TODO: a better way to initialise custom test directives etc
import {} from "@/vue-init";

const factory = () => {
  return mount(FeedbackItemForm);
};

describe("FeedbackItemForm.vue", () => {
  it("renders a form with a required textarea", () => {
    const msg = "Enter text for feedback";

    const wrapper = factory();

    expect(wrapper.text()).toMatch(msg);
    const form = wrapper.get("form");
    expect(form.isVisible()).toBe(true);
    const textarea = wrapper.get("textarea");
    expect(textarea.attributes().required).toBe("required");
  });

  it("when text is empty, send button is disabled", () => {
    const wrapper = factory();
    expect(wrapper.vm.item.text).toMatch("");
    expect(wrapper.get("button").attributes().disabled).toBe("disabled");
  });

  it("when item text has content, textarea is updated and send button is enabled", async () => {
    const wrapper = factory();
    const text = "hello";

    wrapper.vm.item.text = text;
    await wrapper.vm.$nextTick();

    expect(wrapper.get("textarea").element.value).toEqual(text);
    expect(wrapper.get("button").attributes().disabled).toBe(undefined);
  });

  it("when item text contains only whitespace, send button is disabled", async () => {
    const wrapper = factory();
    const text = "    \n   \n";

    wrapper.vm.item.text = text;
    await wrapper.vm.$nextTick();

    expect(wrapper.get("textarea").element.value).toEqual(text);
    expect(wrapper.get("button").attributes("disabled")).toBe("disabled");
  });

  it("when typing in textarea, item text is updated and send button is enabled", async () => {
    const wrapper = factory();
    const text = "i have typed";

    wrapper.get("textarea").setValue(text);
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.item.text).toEqual(text);
    expect(wrapper.get("button").attributes().disabled).toBe(undefined);
  });

  it("when submitting the form we emit an event", async () => {
    const wrapper = factory();
    const text = "What do you think?";
    wrapper.get("textarea").setValue(text);
    await wrapper.vm.$nextTick();

    wrapper.get("form").trigger("submit");
    await wrapper.vm.$nextTick();

    const events = wrapper.emitted("item-submitted");
    expect(events.length).toBe(1);
    expect(events[0][0].text).toBe("What do you think?");
  });
});
