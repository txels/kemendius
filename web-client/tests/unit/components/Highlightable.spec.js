import { mount, shallowMount } from "@vue/test-utils";

import {} from "@/vue-init";
import Highlightable from "@/components/Highlightable.vue";

const factory = (props = {}, shallow = true) => {
  let propsData = {
    text: "<p>Some sample text</p>",
    highlights: [],
    ...props
  };
  if (shallow) {
    return shallowMount(Highlightable, {
      propsData
    });
  } else {
    return mount(Highlightable, {
      propsData
    });
  }
};

describe("Highlightable.vue", () => {
  describe("props:", () => {
    it("contains the text passed as a prop", () => {
      const wrapper = factory();
      let node = wrapper.get("p");

      expect(node.text()).toBe("Some sample text");
    });
    it("contains the highlights passed as a prop", () => {
      const wrapper = factory({ highlights: ["one"] });

      let highlights = wrapper.props("highlights");

      expect(highlights.length).toBe(1);
    });
  });

  describe("methods:", () => {
    it("addMark: adds a mark correctly in the middle of a node", () => {
      const wrapper = factory();
      let node = wrapper.get("p").element;

      let nodesWithMark = wrapper.vm.addMark(node, 0, 5, 11, {});

      expect(nodesWithMark.length).toBe(3);
      expect(nodesWithMark[0].textContent).toBe("Some ");
      let mark = nodesWithMark[1];
      expect(mark.textContent).toBe("sample");
      expect(mark.id).toBe("m-0");
      expect(nodesWithMark[2].textContent).toBe(" text");
    });

    it("addMark: adds a mark correctly at the beginning of a node", () => {
      const wrapper = factory();
      let node = wrapper.get("p").element;

      let nodesWithMark = wrapper.vm.addMark(node, 1, 0, 4, {});

      expect(nodesWithMark.length).toBe(2);
      let mark = nodesWithMark[0];
      expect(mark.textContent).toBe("Some");
      expect(mark.id).toBe("m-1");
      expect(nodesWithMark[1].textContent).toBe(" sample text");
    });

    it("addMark: adds a mark correctly at the end of a node", () => {
      const wrapper = factory();
      let node = wrapper.get("p").element;

      let nodesWithMark = wrapper.vm.addMark(node, 2, 10, 20, {});

      expect(nodesWithMark.length).toBe(2);
      expect(nodesWithMark[0].textContent).toBe("Some sampl");
      let mark = nodesWithMark[1];
      expect(mark.textContent).toBe("e text");
      expect(mark.id).toBe("m-2");
    });

    it("addMark: can work with #text nodes as well", () => {
      const wrapper = factory();
      let node = document.createTextNode("Some sample text");

      let nodesWithMark = wrapper.vm.addMark(node, 3, 10, 20, {});

      expect(nodesWithMark.length).toBe(2);
      expect(nodesWithMark[0].textContent).toBe("Some sampl");
      let mark = nodesWithMark[1];
      expect(mark.textContent).toBe("e text");
      expect(mark.id).toBe("m-3");
    });

    it("addMark: sets correct CSS class if provided", () => {
      const wrapper = factory();
      let node = wrapper.get("p").element;

      let nodesWithMark = wrapper.vm.addMark(node, 0, 5, 11, { thumbs: 1 });

      let mark = nodesWithMark[1];
      expect(mark.outerHTML).toBe(
        '<mark id="m-0" class="highlight-good">sample</mark>'
      );
    });

    it("addMark: adds comment as title if provided", () => {
      const wrapper = factory();
      let node = wrapper.get("p").element;

      let nodesWithMark = wrapper.vm.addMark(node, 0, 5, 11, {
        text: "Some comment"
      });

      let mark = nodesWithMark[1];
      expect(mark.outerHTML).toBe(
        '<mark id="m-0" title="Some comment">sample</mark>'
      );
    });

    it("findAncestorWithId: selects only matching id prefix", () => {
      const wrapper = factory();
      let pNode = document.createElement("p");
      pNode.id = "p-10";
      let markNode = document.createElement("mark");
      markNode.id = "m-10";
      pNode.append(markNode);

      let foundNode = wrapper.vm.findAncestorWithId(markNode, "p");

      expect(foundNode.id).toBe("p-10");
    });

    it("findAncestorWithId: selects any node with id if we give it no prefix", () => {
      const wrapper = factory();
      let pNode = document.createElement("p");
      pNode.id = "p-10";
      let markNode = document.createElement("mark");
      markNode.id = "m-10";
      pNode.append(markNode);

      let foundNode = wrapper.vm.findAncestorWithId(markNode);

      expect(foundNode.id).toBe("m-10");
    });

    it("findAncestorWithId: returns null if no match", () => {
      const wrapper = factory();
      let pNode = document.createElement("p");
      pNode.id = "p-10";
      let markNode = document.createElement("mark");
      markNode.id = "m-10";
      pNode.append(markNode);

      let foundNode = wrapper.vm.findAncestorWithId(markNode, "x");

      expect(foundNode).toBeNull();
    });

    it("createSelection: highlight within one paragraph, no overlaps", () => {
      let highlight = {
        quote: "Some",
        startParagraph: 0,
        endParagraph: 0,
        startOffset: 0,
        endOffset: 4
      };
      const wrapper = factory();

      let selection = wrapper.vm.createSelection(highlight);

      expect(selection.p0.childNodes[0].nodeName).toBe("#text");
      expect(selection.anchorNode).toBe(selection.focusNode);
      expect(selection.anchorNode.textContent).toBe("Some sample text");
      expect(selection.focusNode.textContent).toBe("Some sample text");
    });

    it("createSelection: paragraph with children", () => {
      let highlight = {
        endOffset: 17,
        endParagraph: "0",
        quote: "content",
        startOffset: 10,
        startParagraph: "0",
        text: ""
      };
      const wrapper = factory({ text: "<p>Some <b>bold</b> content</p>" });

      let selection = wrapper.vm.createSelection(highlight);

      expect(selection.p0.childNodes[2].nodeName).toBe("#text");
      expect(selection.anchorNode).toBe(selection.focusNode);
      expect(selection.anchorNode.textContent).toBe(" content");
      expect(selection.anchorOffset).toBe(1);
      expect(selection.focusOffset).toBe(8);
    });

    it("add Node Ids: adds node ids to nested HTML", () => {
      const wrapper = factory({ text: "<ul><li>One</li><li>Two</li></ul>" });

      let el = wrapper.get(".highlightable");
      expect(el.html()).toEqual(
        `<div class="highlightable">
  <ul id="p-0">
    <li id="p-1">One</li>
    <li id="p-2">Two</li>
  </ul>
</div>`
      );
    });

    it("add Node Ids: adds node ids to complex HTML", () => {
      const wrapper = factory({
        text: "<ul><li>One</li><li>Two</li></ul><p>Hello <code>dolly</code></p>"
      });

      let el = wrapper.get(".highlightable");
      expect(el.html()).toEqual(
        `<div class="highlightable">
  <ul id="p-0">
    <li id="p-1">One</li>
    <li id="p-2">Two</li>
  </ul>
  <p id="p-3">Hello <code id="p-4">dolly</code></p>
</div>`
      );
    });

    it("createHighlight: highlight within one paragraph, no overlaps", () => {
      let highlights = [];
      const wrapper = factory({ highlights });
      let p0 = wrapper.element.querySelector("#p-0");
      let textNode = p0.childNodes[0];
      let selection = {
        quote: "sample",
        anchorNode: textNode,
        focusNode: textNode,
        anchorOffset: 5,
        focusOffset: 11
      };

      let { highlight, selectionData } = wrapper.vm.createHighlight(selection);

      expect(highlight).toEqual({
        endOffset: 11,
        endParagraph: "0",
        quote: "sample",
        startOffset: 5,
        startParagraph: "0",
        text: ""
      });
      expect(selectionData.anchorNode.textContent).toEqual("Some sample text");
      expect(selectionData.focusNode).toBe(selectionData.anchorNode);
      expect(selectionData.anchorOffset).toEqual(5);
      expect(selectionData.focusOffset).toEqual(11);
    });

    it("createHighlight: highlight in paragraph with children", () => {
      let highlights = [];
      const wrapper = factory({
        highlights,
        text: "<p>Some <b>bold</b> content</p>"
      });
      let p0 = wrapper.element.querySelector("#p-0");
      let textNode = p0.childNodes[2];
      let selection = {
        quote: "content",
        anchorNode: textNode,
        focusNode: textNode,
        anchorOffset: 1,
        focusOffset: 8
      };

      let { highlight, selectionData } = wrapper.vm.createHighlight(selection);

      expect(highlight).toEqual({
        endOffset: 17,
        endParagraph: "0",
        quote: "content",
        startOffset: 10,
        startParagraph: "0",
        text: ""
      });
      expect(selectionData.anchorNode.textContent).toEqual(" content");
      expect(selectionData.focusNode).toBe(selectionData.anchorNode);
      expect(selectionData.anchorOffset).toEqual(1);
      expect(selectionData.focusOffset).toEqual(8);
    });

    it("displayHighlight: highlight within one paragraph, no overlaps", () => {
      let highlights = [];
      const wrapper = factory({ highlights });
      let p0 = wrapper.element.querySelector("#p-0");
      let textNode = p0.childNodes[0];
      let selectionData = {
        anchorNode: textNode,
        focusNode: textNode,
        anchorOffset: 5,
        focusOffset: 11
      };
      let highlight = {};

      wrapper.vm.displayHighlight(selectionData, highlight);

      expect(wrapper.element.querySelector("p").innerHTML).toEqual(
        'Some <mark id="m-0">sample</mark> text'
      );
    });

    it("add Highlights: highlights in separate paragraphs", () => {
      let highlights = [
        {
          startParagraph: 0,
          endParagraph: 0,
          startOffset: 0,
          endOffset: 3,
          quote: "One"
        },
        {
          startParagraph: 1,
          endParagraph: 1,
          startOffset: 0,
          endOffset: 3,
          quote: "Two"
        }
      ];

      const wrapper = factory({
        text: "<p>One paragraph</p><p>Two paragraphs</p>",
        highlights: highlights
      });

      let el = wrapper.element;
      expect(el.childNodes[0].outerHTML).toEqual(
        '<div class="highlightable">' +
          '<p id="p-0"><mark id="m-0">One</mark> paragraph</p>' +
          '<p id="p-1"><mark id="m-1">Two</mark> paragraphs</p>' +
          "</div>"
      );
    });

    it("add Highlights: highlights in same paragraph", () => {
      let highlights = [
        {
          startParagraph: 0,
          endParagraph: 0,
          startOffset: 0,
          endOffset: 3,
          quote: "One"
        },
        {
          startParagraph: 0,
          endParagraph: 0,
          startOffset: 4,
          endOffset: 13,
          quote: "paragraph"
        }
      ];

      const wrapper = factory({
        text: "<p>One paragraph</p>",
        highlights: highlights
      });

      let el = wrapper.element;
      expect(el.childNodes[0].outerHTML).toEqual(
        '<div class="highlightable">' +
          '<p id="p-0"><mark id="m-1">One</mark> <mark id="m-0">paragraph</mark></p>' +
          "</div>"
      );
    });

    it("getCssClass: based on thumbs", () => {
      const wrapper = factory();

      expect(wrapper.vm.getCssClass({ thumbs: 1 })).toEqual("highlight-good");
      expect(wrapper.vm.getCssClass({ thumbs: -11 })).toEqual("highlight-bad");
    });
  });

  describe("the highlight popup menu", () => {
    const wrapper = factory(
      {
        menu: {}
      },
      false
    );

    it("can be shown", async () => {
      wrapper.setData({
        showMenu: true
      });
      await wrapper.vm.$nextTick();
      expect(wrapper.html()).toContain("highlight-menu");
    });

    it("is no longer shown upon clicking close", async () => {
      wrapper.get("#menu-close").trigger("click");
      await wrapper.vm.$nextTick();

      expect(wrapper.html()).not.toContain("highlight-menu");
    });
  });
});
