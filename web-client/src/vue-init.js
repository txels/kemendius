import Vue from "vue";
import Meta from "vue-meta";

import BaseIcon from "@/components/BaseIcon.vue";
import BaseSubmit from "@/components/BaseSubmit.vue";

// turn some of these into plugins?
// https://vuejs.org/v2/guide/plugins.html#Writing-a-Plugin
Vue.prototype.$window = window;
Vue.prototype.$document = document;

Vue.component("BaseIcon", BaseIcon);
Vue.component("BaseSubmit", BaseSubmit);
Vue.use(Meta);

Vue.directive("focus", {
  inserted: function(el) {
    el.focus();
  }
});
