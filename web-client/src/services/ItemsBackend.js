import axios from "axios";
import * as settings from "./settings";

// TODO: find a better way to do environment-specific settings?
// TODO: resiliency patterns - https://github.com/softonic/axios-retry
// rather than increased timeouts
const backend = axios.create({
  baseURL: settings.BASE_URL,
  timeout: 8000,
  withCredentials: false
  // headers: {
  //   Accept: "application/json",
  //   "Content-Type": "application/json"
  // }
});

export default {
  async getItem(itemId, type = "work") {
    let response = await backend.get(`/${type}/${itemId}`);
    // TODO: populate comments at the backend
    response.data.comments = [];
    return response;
  },
  postItem(item) {
    return backend.post("/work", item);
  },
  getComments(itemId) {
    return backend.get(`/work/${itemId}/comments`);
  },
  postComment(itemId, highlight) {
    return backend.post(`/work/${itemId}/comments`, highlight);
  },
  uploadFile(data, config) {
    return backend.post("/file", data, config);
  },
  getFileUrl(id) {
    return `${backend.defaults.baseURL}/file/${id}/data`;
  }
};
