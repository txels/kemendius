export const IS_LOCALHOST =
  ["localhost", "127.0.0.1", "192.168.1."].findIndex(x =>
    window.location.hostname.startsWith(x)
  ) !== -1;

export const HOST = IS_LOCALHOST ? "localhost:8901" : "api.kemendius.com";

export const BASE_URL = `//${HOST}`;
