import * as settings from "./settings";

const PROTOCOL = settings.IS_LOCALHOST ? "ws" : "wss";
let connected = false;

const connect = (id, onmessage) => {
  let socket = new WebSocket(`${PROTOCOL}://${settings.HOST}/updates/${id}`);
  socket.onopen = () => {
    connected = true;
    // console.log("connected");
  };
  socket.onclose = () => {
    // console.log("reconnecting");
    setTimeout(() => {
      connect();
    }, 1000);
  };
  socket.onerror = () => {
    // console.log("error");
  };
  socket.onmessage = onmessage;
  return socket;
};

const send = (socket, message) => {
  if (connected) {
    socket.send(message);
  }
};

export default {
  connect,
  send
};
