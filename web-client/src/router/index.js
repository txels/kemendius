import Vue from "vue";
import VueRouter from "vue-router";
import NProgress from "nprogress";

import AboutPage from "@/views/AboutPage.vue";
import AddTextPage from "@/views/AddTextPage.vue";
import TextFeedbackPage from "@/views/TextFeedbackPage.vue";
import NotFoundPage from "@/views/NotFoundPage.vue";
import AddFilePage from "@/views/AddFilePage.vue";
import ImageFeedbackPage from "@/views/ImageFeedbackPage.vue";

Vue.use(VueRouter);

const UUID =
  "[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}";

const routes = [
  {
    path: "/",
    name: "about",
    component: AboutPage
  },
  {
    path: "/text",
    name: "add-text",
    component: AddTextPage
  },
  {
    path: `/text/:id(${UUID})`,
    name: "text",
    component: TextFeedbackPage,
    props: true
  },
  {
    path: `/item/:id(${UUID})`,
    name: "item",
    component: TextFeedbackPage,
    props: true
  },
  {
    // Deprecated, redirects to new path
    path: `/:id(${UUID})`,
    redirect: { name: "text" }
  },
  {
    path: "/image",
    name: "add-image",
    component: AddFilePage
  },
  {
    path: `/image/:id(${UUID})`,
    name: "image",
    component: ImageFeedbackPage,
    props: true
  },
  { path: "*", component: NotFoundPage }
];

const router = new VueRouter({
  // mode: "history", // TODO: set up nginx in the dockerfile for this
  // https://router.vuejs.org/guide/essentials/history-mode.html#nginx
  routes
});

router.beforeEach((routeFrom, routeTo, next) => {
  NProgress.start();
  next();
});

router.afterEach(() => {
  NProgress.done();
});

export default router;
