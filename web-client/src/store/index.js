import Vue from "vue";
import Vuex from "vuex";

import * as config from "./config";

Vue.use(Vuex);

export default new Vuex.Store(config);
