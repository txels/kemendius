import * as feedback from "./modules/feedback";
import * as notification from "./modules/notification.js";

export const modules = { feedback, notification };

export const state = {};
