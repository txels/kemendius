import marked from "marked";

import ItemsBackend from "@/services/ItemsBackend";

export const namespaced = true;

const EMPTY_ITEM = { comments: [] };

export const state = {
  items: {}, // all items, mapped by id -> item
  item: EMPTY_ITEM // the current item we are working with
};

export const mutations = {
  ADD_ITEM(state, item) {
    const newItems = { ...state.items };
    newItems[item.id] = item;
    state.items = newItems;
  },
  SET_ITEM(state, item) {
    state.item = item;
  },
  ADD_COMMENT(state, comment) {
    state.item.comments = [...state.item.comments, comment];
  }
};

export const actions = {
  clearItem({ commit }) {
    commit("SET_ITEM", EMPTY_ITEM);
  },
  async loadItem({ commit, dispatch }, { id, type }) {
    try {
      let response = await ItemsBackend.getItem(id, type);
      let item = response.data;
      // TODO: maybe item should be a class instance (based on
      // type of item: text/image), and `rendered` a property
      if (item.text) {
        item.rendered = marked(item.text);
      }
      response = await ItemsBackend.getComments(id);
      item.comments = response.data;
      item.comments.map(comment => {
        comment.path = comment.path ? JSON.parse(comment.path) : null;
      });
      commit("ADD_ITEM", item);
      commit("SET_ITEM", item);
    } catch (e) {
      const notification = {
        type: "error",
        message: `There was a problem fetching the item ${id}: `,
        exception: e
      };
      dispatch("notification/add", notification, { root: true });
      dispatch("clearItem");
    }
  },
  async createItem({ commit }, item) {
    let response = await ItemsBackend.postItem(item);
    item.id = response.data;
    commit("ADD_ITEM", item);
  },
  async createComment({ state, commit }, comment) {
    // cloneComment that we manipulate before sending
    // TODO: serialize and deserialize at backend...
    let cloneComment = Object.assign({}, comment);
    cloneComment.path = JSON.stringify(cloneComment.path);
    let response = await ItemsBackend.postComment(state.item.id, cloneComment);
    comment.id = response.data;
    commit("ADD_COMMENT", comment);
  },
  pushComment({ commit }, comment) {
    commit("ADD_COMMENT", comment);
  }
};
