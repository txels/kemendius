# 2020

## Goals for minimalistic MVP

Frontend:

- [x] Programmatically create a selection
- [x] Post highlights to the backend
- [x] Display highlights upon page load

Backend:

- [x] Add highlights endpoint

...share with a small cohort of friends.

- [x] Català: Arnau, Carles, Joan, Bea
- [x] English: ...?
      [x] Alex C,
      [x] Simon Booth,
      [x] Matteo R,
      [x] Grant H,
      [x] Ondrej

## MVP Feedback

- [x] Derive actions from feedback:
  - [x] Markdown
  - [x] Add comment to highlight
  - [.] Feedback as a "poll"... (?)

## MVP refactor

- [x] Decouple highlightable from markdown
- [x] Clean FE architecture: no hardcoded backend dependency on components
- [ ] Write down the architectural guidelines and adhere to them

## A more complete MVP - to share more broadly

Frontend:

- [x] About page
- [x] Add thumbs-up/down to highlight: pass menu options as props
- [x] Colored highlights
- [x] Add comment to highlight
- [x] Refine removing popup menu: e.g. https://stackoverflow.com/questions/36170425/detect-click-outside-element
- [ ] Double submission bug?
- [ ] Deal with multi-node text highlights
- [ ] Deal with overlapping text highlights
- [.] Live updates e.g. [websockets](https://github.com/nathantsoi/vue-native-websocket) and https://quarkus.io/guides/websockets
  - [x] For text feedback
  - [ ] Ensure active viewer count gets updated upon reconnect
  - [ ] For image feedback (unify "feedback" pages)
- [ ] Global keyboard events (copy the idea from ployst-ui)

- [ ] Use proper ids for highlight objects (make highlight a class?)
- [ ] Owner can edit "submission" (maybe special edit link?)
- [ ] Edit a comment
- [ ] Remove a comment
- [ ] Menu for existing highlights: thumbs, delete...?
- [ ] Remove a highlight
- [ ] Proper "title" based on submission. Have users "add titles to submissions" vs "infer title from first line"? https://github.com/nuxt/vue-meta
- [ ] Meta tags for social sharing
- [ ] Add credits to about page: vue, quarkus, tailwind, icons...
- [ ] try making a Svelte/Sapper version?
- [ ] history mode

```
https://router.vuejs.org/guide/essentials/history-mode.html
# nginx:

location / {
  try_files $uri $uri/ /index.html;
}
```

- File upload: https://quarkus.io/guides/rest-client-multipart and https://github.com/axios/axios/blob/master/examples/upload/index.html - https://serversideup.net/uploading-files-vuejs-axios/

Backend:

- [x] Missing item should be 404 not 500
- [ ] Single endpoint to retrieve item with all comments?

Operational:

- [x] Set readiness and liveness probes
- [x] SSL: https://cert-manager.io/docs/ https://akomljen.com/get-automatic-https-with-lets-encrypt-and-kubernetes-ingress/ https://docs.bitnami.com/kubernetes/how-to/secure-kubernetes-services-with-ingress-tls-letsencrypt/
- [ ] Deploy versions rather than `latest` - use helm for that
- [ ] Use vue CLI modes properly https://cli.vuejs.org/guide/mode-and-env.html#modes
- [ ] Set up SSR? (https://ssr.vuejs.org/) (move to nuxt?)
- [ ] Better dockerisation with nginx config in version control
- [ ] Logging (https://docs.fluentd.org/v/0.12/container-deployment/kubernetes) or (https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes)
- [ ] Metrics (e.g. grafana, prometheus...)

## Further features (pending validation and prioritisation)

- [ ] i18n
- [ ] Let user add meta info as "message for reviewer"
- [ ] Use a database as a data backend? https://quarkus.io/guides/hibernate-orm-panache - or use git?
- [ ] Use django/rust/elixir/... as a backend?
- [ ] Other forms of feedback: global thumbs up/down, star rating, traffic light... polls.
- [ ] Custom extensions to markdown? migrate to remarkable? https://github.com/jonschlinkert/remarkable
- [ ] Revisions (versioning of submissions)

Features for registered users:

- [ ] User registration: https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete
- [ ] Share: Email / others
- [ ] Nominal feedback
- [ ] Notifications (when you receive feedback)
