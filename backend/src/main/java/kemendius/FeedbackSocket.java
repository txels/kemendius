package kemendius;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/updates/{id}", encoders = { JsonEncoder.class })
@ApplicationScoped
public class FeedbackSocket {

    Map<String, List<Session>> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("id") String id) {
        if (!sessions.containsKey(id)) {
            sessions.put(id, Collections.synchronizedList(new ArrayList<Session>()));
        }
        List<Session> sessionList = sessions.get(id);
        sessionList.add(session);
        sendSessionEvent("join", id);
    }

    @OnClose
    public void onClose(Session session, @PathParam("id") String id) {
        if (sessions.containsKey(id)) {
            List<Session> sessionList = sessions.get(id);
            sessionList.remove(session);
            sendSessionEvent("leave", id);
        }
    }

    @OnError
    public void onError(Session session, @PathParam("id") String id, Throwable throwable) {
        onClose(session, id);
    }

    @OnMessage
    public void onMessage(String message, @PathParam("id") String id) {
        send(id, message);
    }

    private void sendSessionEvent(String event, String id) {
        List<Session> sessionList = sessions.get(id);
        if (sessionList != null) {
            //@formatter:off
            JsonObject message = Json.createObjectBuilder()
                .add("event", event)
                .add("id", id)
                .add("sessions", sessionList.size())
                .build();
            //@formatter:on
            send(id, message);
        }
    }

    private void send(String id, Object message) {
        List<Session> sessionList = sessions.get(id);
        if (sessionList != null) {
            sessionList.forEach(s -> s.getAsyncRemote().sendObject(message, result -> {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            }));
        }
    }

}