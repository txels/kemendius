package kemendius.models;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Base64.Decoder;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

class BytesDeserializer extends JsonDeserializer<byte[]> {
    final Decoder decoder = Base64.getDecoder();

    @Override
    public byte[] deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        String text = jp.getText().trim();
        return decoder.decode(text);
    }
}

public class UploadedFile extends Contribution {
    public static final Charset UTF8 = Charset.forName("UTF-8");

    public UploadedFile() {
    }

    public UploadedFile(String name, byte[] body) {
        this.name = name;
        this.body = body;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public byte[] getBody() {
        return this.body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    @FormParam("name")
    @PartType(MediaType.TEXT_PLAIN)
    private String name = null;

    @FormParam("type")
    @PartType(MediaType.TEXT_PLAIN)
    private String type = null;

    @FormParam("body")
    // @JsonIgnore
    // @JsonSerialize(using = BytesSerializer.class)
    // @JsonDeserialize(using = BytesDeserializer.class)
    private byte[] body = null;

}