package kemendius.models;

/**
 * A comment on a piece of work
 */
public class Comment {

    public Comment(String workId, String text) {
        this.workId = workId;
        this.text = text;
        this.createId();
    }

    public Comment() {
        this(null, null);
    };

    public String getWorkId() {
        return this.workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id == null) {
            createId();
        } else {
            this.id = id;
        }
    }

    private void createId() {
        this.id = Long.toHexString(System.currentTimeMillis());
    }

    public int getStartParagraph() {
        return startParagraph;
    }

    public void setStartParagraph(int startParagraph) {
        this.startParagraph = startParagraph;
    }

    public int getEndParagraph() {
        return endParagraph;
    }

    public void setEndParagraph(int endParagraph) {
        this.endParagraph = endParagraph;
    }

    public int getStartOffset() {
        return startOffset;
    }

    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return endOffset;
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String highlight) {
        this.quote = highlight;
    }

    public int getThumbs() {
        return thumbs;
    }

    public void setThumbs(int thumbs) {
        this.thumbs = thumbs;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    private String workId;
    private String text;
    private String id;
    private String quote;
    private String color;
    private String path;
    private int thumbs;
    private int startParagraph;
    private int endParagraph;
    private int startOffset;
    private int endOffset;
}
