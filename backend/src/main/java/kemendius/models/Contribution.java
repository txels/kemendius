package kemendius.models;

import java.util.UUID;

public abstract class Contribution {

    Contribution() {
        this.createId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id == null) {
            createId();
        } else {
            this.id = id;
        }
    }

    String createId() {
        id = UUID.randomUUID().toString();
        return id;
    }

    private String id;
}