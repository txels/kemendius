package kemendius.models;

public class TextItem extends Contribution {

    public TextItem(String content) {
        super();
        this.text = content;
    }

    public TextItem() {
        this(null);
    };

    public String getText() {
        return this.text;
    }

    public void setText(String content) {
        this.text = content;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TextItem other = (TextItem) obj;
        if (text == null) {
            if (other.text != null)
                return false;
        } else if (!text.equals(other.text))
            return false;
        return true;
    }

    private String text;

}
