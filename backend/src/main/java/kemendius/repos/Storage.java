package kemendius.repos;

import java.io.IOException;

import kemendius.models.Comment;
import kemendius.models.Contribution;

public interface Storage {
    public <T> T get(String id, Class<T> clazz) throws IOException;

    public byte[] get(String id) throws IOException;

    public String put(String id, byte[] rawData) throws IOException;

    public String put(Contribution item) throws IOException;

    public String put(Comment comment) throws IOException;

    public Iterable<Comment> getAll(String workid) throws IOException;
}
