package kemendius;

import java.io.IOException;
import java.nio.file.NoSuchFileException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import kemendius.models.UploadedFile;
import kemendius.repos.Storage;

@Path("/file")
public class FileResource {
    private static final Logger LOG = Logger.getLogger(FileResource.class);

    @Inject
    Storage storage;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String createFile(@MultipartForm final UploadedFile file) throws Exception {
        byte[] body = file.getBody();
        file.setBody(null);
        storage.put(file);
        storage.put(file.getId(), body);
        LOG.info(String.format("Uploaded file: %s (%d bytes)", file.getName(), body.length));
        return file.getId();
    }

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UploadedFile getFile(@PathParam("id") final String id) throws IOException {
        try {
            return storage.get(id, UploadedFile.class);
        } catch (final NoSuchFileException e) {
            throw new NotFoundException("Not found");
        }
    }

    @Path("/{id}/data")
    @GET
    public Response getFileData(@PathParam("id") final String id) throws IOException {
        try {
            UploadedFile file = storage.get(id, UploadedFile.class);
            final byte[] body = storage.get(id);

            return Response.status(Response.Status.OK).entity(body).type(file.getType()).build();
        } catch (final NoSuchFileException e) {
            throw new NotFoundException("Not found");
        }
    }
}