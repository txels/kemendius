package kemendius;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.vertx.core.json.JsonObject;
import kemendius.models.Comment;

//@formatter:off
@QuarkusTest
public class CommentResourceTest {

    @Test
    void youGetWhatYouPut() {
        JsonObject data = new JsonObject();
        data.put("quote", "is putting");
        data.put("text", "Is he really?");
        data.put("startParagraph", 0);
        data.put("startOffset", 7);
        data.put("endParagraph", 0);
        data.put("endOffset", 17);
        data.put("thumbs", -1);
        Response postCommentResponse = given()
                .contentType(ContentType.JSON)
                .body(data.toString())
            .when()
                .post("/work/dummy-uuid/comments/")
            .then()
                .statusCode(200)
                .extract()
                .response();

        String id = postCommentResponse.getBody().asString();
        ValidatableResponse response = given()
            .when()
                .get("/work/dummy-uuid/comments/{id}", id)
            .then()
                .statusCode(200);
        response.body("text", equalTo(data.getString("text")));
        response.body("quote", equalTo(data.getString("quote")));
        response.body("startParagraph", equalTo(data.getInteger("startParagraph")));
        response.body("startOffset", equalTo(data.getInteger("startOffset")));
        response.body("endParagraph", equalTo(data.getInteger("endParagraph")));
        response.body("endOffset", equalTo(data.getInteger("endOffset")));
        response.body("thumbs", equalTo(data.getInteger("thumbs")));
    }

    @Test
    void getAllComments() {
        String uuid = UUID.randomUUID().toString();

        JsonObject data = new JsonObject();
        data.put("workId", uuid);
        data.put("text", "Carles is putting stuff");
        Response postCommentResponse = given()
                .contentType(ContentType.JSON)
                .body(data.toString())
            .when()
                .post("/work/{id}/comments/", uuid)
            .then()
                .statusCode(200)
                .extract()
                .response();

        String id = postCommentResponse.getBody().asString();
        Response allCommentsResponse = given()
            .when()
                .get("/work/{id}/comments/", id)
            .then()
                .statusCode(200)
                .extract()
                .response();

        Comment[] response = allCommentsResponse.body().as(Comment[].class);
        for (Comment c : response) {
            assertEquals("no comment", c.getText());
        }
    }
}
