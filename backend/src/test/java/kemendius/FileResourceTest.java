package kemendius;

import static io.restassured.RestAssured.given;
import static kemendius.models.UploadedFile.UTF8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.ResponseBody;
import kemendius.models.UploadedFile;
import kemendius.repos.FileStorage;

//@formatter:off
@QuarkusTest
public class FileResourceTest {

    @Inject
    FileStorage storage;

    @Test
    void youGetWhatYouPut() throws IOException {
        final byte[] bodyBytes = "body".getBytes(UTF8);
        storage.put("sample-text", bodyBytes);
        ResponseBody<?> body = given()
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .multiPart("name", "image")
            .multiPart("type", "image/jpeg")
            .multiPart("body", new File(storage.getLocation() + File.separator + "sample-text.raw"))
            .when()
                .post("/file")
            .then()
                .statusCode(200)
                .extract()
                .response()
                .getBody();

        final UUID uuid = UUID.fromString(body.asString());
        final UploadedFile upload = storage.get(uuid.toString(), UploadedFile.class);
        final byte[] data = storage.get(uuid.toString());

        assertThat(upload.getName(), equalTo("image"));
        assertThat(data, equalTo(bodyBytes));

        body = given()
            .when()
                .get("/file/" + uuid.toString() + "/data")
            .then()
                .contentType("image/jpeg")
                .statusCode(200)
                .extract()
                .response()
                .getBody();

        assertThat(
            new String(body.asByteArray(), UTF8), equalTo("body")
        );
    }

}
