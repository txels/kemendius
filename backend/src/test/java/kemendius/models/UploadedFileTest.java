package kemendius.models;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class UploadedFileTest {
    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void serialize() throws JsonProcessingException {
        UploadedFile original = new UploadedFile("hello", "hello".getBytes(UploadedFile.UTF8));
        String jsonItem = mapper.writeValueAsString(original);
        UploadedFile decoded = mapper.readValue(jsonItem, UploadedFile.class);

        assertThat(decoded.getName(), equalTo(original.getName()));
    }
}
